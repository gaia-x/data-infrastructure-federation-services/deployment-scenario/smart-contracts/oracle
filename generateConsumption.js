const cron = require('node-cron')
const { ethers, JsonRpcProvider } = require("ethers");
const axios = require('axios')

require("dotenv").config();

const scManagerUrl = process.env.SC_MANAGER_URL

//  node .\generateConsumption.js 0x95401dc811bb5740090279Ba06cfA8fcF6113778 http://127.0.0.1:8545 0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80 CSV XML

const [address, ...types] = process.argv.slice(2);

async function getContractABI(address) {
    try {
        const result = await axios.get(`${scManagerUrl}/smartContracts?address=${address}`);
        return JSON.parse(result.data.infos.abi)
    } catch (error) {
        throw new HttpError(`Error fetching contract ABI: ${error.message}`, 500);

    }
}

async function getContractConnection(sc) {
    const provider = new JsonRpcProvider(process.env.EVM_NODE_URL);
    const signer = new ethers.Wallet(process.env.PRIVATE_KEY, provider);
    const contractABI = await getContractABI(sc.address);

    const contract = new ethers.Contract(sc.address, contractABI, signer);
    return contract;
}

async function SaveConsumption(sc, consumption) {
    try {
        const contract = await getContractConnection(sc)
        const tx = await contract.writeData(consumption);
        const receipt = await tx.wait();
        console.log('Consumption data written to the blockchain: ', consumption);
    } catch (error) {
        throw new Error(`Error writing Consumption in blockchain: ${error.message}`);
    }
}

function getRandomSubarray(arr) {
    let subarraySize = Math.floor(Math.random() * arr.length) + 1;
    return arr
        .sort(() => 0.5 - Math.random())
        .slice(0, subarraySize)
        .map(item => [item, Math.floor(new Date().getTime() / 1000), Math.floor(Math.random() * 100) + 1]);
}

let previous = new Date();
cron.schedule("*/8 * * * * *", () => {
    let randomizedConsumption = getRandomSubarray(types)
    console.log("New consumption:");
    console.log(randomizedConsumption);
    try {
        SaveConsumption({ address: address }, randomizedConsumption);
    } catch (e) {
        console.log(e)
    }
});