/* For local development */
if (process.env.NODE_ENV !== "production") require("dotenv").config();

const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const logger = require('./services/logger');
const HttpError = require('./utils/httpError');
const checkSchema = require('express-validator').checkSchema
const schema = require('./utils/schema')
const validateSchema = require('./utils/validateSchema')
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../openapi.json');


const paymentSCService = require('./services/paymentSmartContract');
const scheduledPaymentService = require('./services/scheduledPayment');
const scheduledBillingService = require('./services/scheduledBilling');


const app = express();
const PORT = 4004;
app.use(bodyParser.json());
app.use(cors());

/* Error */
app.use((error, req, res, next) => {
    logger.error(error.message, { stackTrace: error.stack ? error.stack : 'No stack trace' });
    if (error instanceof HttpError && error.statusCode !== 500) {
        res.status(error.statusCode).json({ error: error.message });
    } else {
        res.status(500).json({ error: 'Internal server error' });
    }
});

//swagger
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// payment
app.post('/payment/cron', checkSchema(schema['/payment/cron'], ['body']), validateSchema, (req, res) => {
    scheduledPaymentService.startCronJob(req);
    res.status(200).json({ message: `Cron job started with periodicity: ${req.body.periodicity}` });
});

app.post('/payment', checkSchema(schema['POST /payment'], ['body']), validateSchema, (req, res) => {
    scheduledPaymentService.savePaymentManually(req, () => {
        res.status(200).json({ message: 'Payment created manually' });
    })
});

app.get('/payment', checkSchema(schema['GET /payment'], ['query']), validateSchema, (req, res) => {
    scheduledPaymentService.getPayments(req, (result) => {
        res.status(200).json(result);
    });
});
app.post('/confirmPayment', checkSchema(schema['POST /confirmPayment'], ['query']), validateSchema, (req, res) => {
    paymentSCService.confirmPayment({
        address: req.query.address
    }, req.query.paymentId, () => {
        res.status(200).json({ message: 'Payment confirmed successfully' });
    });
});


//bill
app.post('/bill/cron', checkSchema(schema['/bill/cron'], ['body']), validateSchema, (req, res) => { 
    scheduledBillingService.startCronJob(req, () => {
        res.status(200).json({ message: `Cron job started with periodicity: ${req.body.periodicity}` });
    });
})

app.post('/bill', checkSchema(schema['/bill'], ['body']), validateSchema, (req, res) => {
    scheduledBillingService.saveBillManually(req, () => {
        res.status(200).json({ message: 'Bill created manually' });
    })
})

app.get('/bill', (req, res) => {
    scheduledBillingService.getBills(req, (result) => { 
        res.status(200).json(result);
    });
})
// here
app.post('/bill/period', checkSchema(schema['/bill/period'], ['body']), validateSchema, (req, res) => {
    scheduledBillingService.getBillsByPeriod(req, (result) => {
        res.status(200).json(result);
    });
})

app.post('/bill/currency', checkSchema(schema['/bill/currency'], ['body']), validateSchema, (req, res) => {
    scheduledBillingService.setCurrency(req, () => {
        res.status(200).json({ message: `Currency successfully set` });
    });
})

app.post('/bill/pricing', checkSchema(schema['/bill/pricing'], ['body']), validateSchema, (req, res) => {
    scheduledBillingService.setPricing(req, () => {
        res.status(200).json({ message: `Pricing successfully set` });
    });
})

app.post('/consumption', (req, res) => {
    consumptionSCService.SaveConsumption({ contractDId: req.body.contractDId }, req.body.data);
    res.status(200).json({ message: `Consumption successfully saved in blockchain` });
});

app.get('/consumption', (req, res) => {
    consumptionSCService.GetConsumption2({ contractDId: req.query.contractDId }, req.query.startDate, req.query.endDate, (result) => {
        console.log(result);
    });
    res.status(200).json({ message: `Consumption successfully saved in blockchain` });
})


// Start the server
app.listen(PORT, () => {
    logger.info(`server started at port ${PORT}`);
});
