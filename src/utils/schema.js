const web3Utils = require('web3').utils

function isWeb3Address(data) {
    console.log(web3Utils.isAddress(data))
    return (web3Utils.isAddress(data))
}

const schema = {
    '/bill': {
        'address': {
            addressIsValid: {
                custom: isWeb3Address,
                errorMessage: 'address must be a SC address',
            }
        },
        'startDate': {isISO8601: true},
        'endDate': {isISO8601: true},
        'consumption': {exists: true}
    },
    '/bill/cron': {
        'periodicity': {isString: true}, 
        'address': {
            addressIsValid: {
                custom: isWeb3Address,
                errorMessage: 'address must be a SC address',
            }
        }
    },
    '/bill/currency': {
        'address': {
            addressIsValid: {
                custom: isWeb3Address,
                errorMessage: 'address must be a SC address',
            }
        },
        'currency': {isString: true}
    },
    '/bill/pricing': {
        'address': {
            addressIsValid: {
                custom: isWeb3Address,
                errorMessage: 'address must be a SC address',
            }
        },
        'pricing': {exists: true}
    },
    '/bill/period': {
        'address': {
            addressIsValid: {
                custom: isWeb3Address,
                errorMessage: 'address must be a SC address',
            }
        },
        'startDate': {isISO8601: true},
        'endDate': {isISO8601: true},
    },
    '/payment/cron': {
        'periodicity': {isString: true}, 
        'address': {
            addressIsValid: {
                custom: isWeb3Address,
                errorMessage: 'address must be a SC address',
            }
        },
        'paymentMethod':  {isString: true},
        'paymentSolution': {isString: true},
        'debitorEmail':  {isString: true}
    },
    'GET /payment': {
        'address': {
            addressIsValid: {
                custom: isWeb3Address,
                errorMessage: 'address must be a SC address',
            }
        },
    },
    
    'POST /payment': {
        'address': {
            addressIsValid: {
                custom: isWeb3Address,
                errorMessage: 'address must be a SC address',
            }
            
        },
        'paymentMethod':  {isString: true},
        'paymentSolution': {isString: true},
        'debitorEmail':  {isString: true}
    },
    'POST /confirmPayment': {
        'address': {
            addressIsValid: {
                custom: isWeb3Address,
                errorMessage: 'address must be a SC address',
            }

        },
        'paymentId':  {isString: true}
    }
}

module.exports = schema;
