module.exports = (interval) => {
    const isoRegex = /^P(?:(\d+Y)?(\d+M)?(\d+W)?(\d+D)?)?(T(?:(\d+H)?(\d+M)?(\d+S)?))?$/;

    const match = interval.match(isoRegex);
    if (!match) {
        throw new Error('Invalid ISO8601 interval');
    }

    const [, years, months, weeks, days, hours, minutes, seconds] = match.slice(1).map(val => parseInt(val) || 0);

    let second = "*";
    let minute = '*';
    let hour = '*';
    let dayOfMonth = '*';
    let month = '*';
    let dayOfWeek = '*';

    if (days !== 0) {
        dayOfMonth = `*/${days}`;
    } else if (weeks !== 0) {
        dayOfWeek = `*/${weeks}`;
    }

    if (minutes !== 0) {
        minute = `*/${minutes}`;
    }

    if (hours !== 0) {
        hour = `*/${hours}`;
    }

    if (months !== 0) {
        month = `*/${months}`;
    }

    if (seconds !== 0) {
        second = `*/${seconds}`;
    }

    return `${second !== '*' ? second : ''} ${minute} ${hour} ${dayOfMonth} ${month} ${dayOfWeek}`;
}