const cron = require('node-cron')
const axios = require('axios')
const HttpError = require("../utils/httpError");
paymentSCService = require('../services/paymentSmartContract');
billingSCService = require('../services/billingSmartContract');
const isoToCron = require("../utils/isoToCron");

const paymentGatewayBaseUrl = process.env.PAYMENT_GATEWAY_URL

const mockPaidInvoices = [
    {
        id: 'invoice1',
        amount: 100
    },
    {
        id: 'invoice2',
        amount: 200
    },
    {
        id: 'invoice3',
        amount: 300
    }
]

async function paymentProcess(sc, options, startDate, endDate) {
    try {
        // console.log(startDate, endDate);
        // options.invoices = [
        //     {
        //         id: 'invoice1',
        //         amount: 100,
        //     },
        //     {
        //         id: 'invoice2',
        //         amount: 200
        //     },
        //     {
        //         id: 'invoice3',
        //         amount: 300
        //     }
        // ]

        if (1) { //!options.invoices
            options.invoices = await fetchInvoicesFromBlockchain(sc, startDate, endDate);
        }

        const paymentBC = await paymentSCService.savePayment(sc, options.invoices, options.paymentSolution);

        options.invoice = {
            amount: paymentBC.amount,
            debitorEmail: options.debitorEmail,
            callBackURL: `http://localhost:4004/confirmPayment?paymentId=${paymentBC.paymentId}&did=${sc.contractDId}`
            //callBackURL: `${req.protocol}://${req.get('host')}/confirmPayment/${paymentBC.paymentId}`

        };
        if ( options.invoice.amount === 0 ) {
            console.log("Payment amount is 0, skipping ...");
            return;
        }
        console.log(options.invoice.callBackURL);
        const paymentResponse = await axios.post(`${paymentGatewayBaseUrl}/${options.paymentMethod}/${options.paymentSolution}`, options);

        await paymentSCService.updateExternalId(sc, paymentBC.paymentId, paymentResponse.data.paidInvoice.idExternal);

    } catch (error) {
        console.log('Error processing payments:', error.message)
    }
}

async function fetchInvoicesFromBlockchain(sc, startDate, endDate) {
    console.log("fetching invoices from blockchain")
    const billingInfos = await paymentSCService.getLinkedBilling(sc.contractDId);
    const invoices = await billingSCService.getBillsByPeriod(billingInfos,
        startDate, endDate);
    console.log("fetched invoices :")
    console.log(invoices)
        
    return invoices;
}

function startCronJob(req) {
    console.log("payment cron loop ...")
    const cronExpression = isoToCron(req.body.periodicity);
    if (!cronExpression) {
        throw new Error('Invalid periodicity')
    }
    let previous = new Date()
    cron.schedule(cronExpression, () => {
        let now = new Date()
        console.log(`Scheduled task started (${req.body.periodicity}). Calling payment gateway...`)
        console.log("#########################",req.body);
        paymentProcess({ address: req.body.address, contractDId: req.body.contractDId },
            { ...req.body }, previous, now)
        previous = new Date()
    });
}

async function getPayments(req, next) {
    try {
        const data = await paymentSCService.getPayments({ address: req.query.address });
        console.log("data")
        console.log(data)
        next(data);
    } catch (error) {
        console.error('Error getting payments:', error.message)
        throw new HttpError('Error getting payments', 500);

    }
}

async function savePaymentManually(req, next) {
    try {
        await paymentSCService.savePayment({ address: req.body.address },
            mockPaidInvoices
        );
        next();
    } catch (error) {
        console.error('Error saving payment manually:', error.message)
        throw new HttpError('Error saving payment manually', 500);
    }
}

module.exports = {
    getPayments,
    startCronJob,
    savePaymentManually
}
