const logger = require('./logger');
const { ethers, JsonRpcProvider } = require("ethers");
const HttpError = require("../utils/httpError");
const axios = require('axios')
const scManagerUrl = process.env.SC_MANAGER_URL

async function getContractABI(contractDId) {
    try {
        const result = await axios.get(`${scManagerUrl}/smartContracts?contractDId=${contractDId}&label=payment`);

        return JSON.parse(result.data.infos.abi)
    } catch (error) {
        throw new HttpError(`Error fetching contract ABI: ${error.message}`, 500);

    }
}
async function getLinkedBilling(contractDId) {
    try {
        console.log(`${scManagerUrl}/smartContracts?contractDId=${contractDId}&label=billing`);
        const result = await axios.get(`${scManagerUrl}/smartContracts?contractDId=${contractDId}&label=billing`);

        return result.data.infos
    } catch (error) {
        throw new HttpError(`Error fetching linked Billing: ${error.message}`, 500);

    }
}

async function getContractConnection(sc) {
    const provider = new JsonRpcProvider(process.env.EVM_NODE_URL);
    const signer = new ethers.Wallet(process.env.PRIVATE_KEY, provider);
    const contractABI = await getContractABI(sc.contractDId);

    return new ethers.Contract(sc.address, contractABI, signer);
}

async function savePayment(sc, invoices, paymentSolution) {
    try {
        const ids = [];
        const amounts = [];

        for (const invoice of invoices) {
            ids.push(invoice.id.toString())
            amounts.push(invoice.totalAmount)
        }

        const contract = await getContractConnection(sc)
        console.log(ids, amounts, paymentSolution);
        const tx = await contract.savePayment(ids, amounts, paymentSolution);
        const receipt = await tx.wait();

        // Check if logs are present in the transaction receipt
        if (!receipt.logs || receipt.logs.length === 0) {
            throw new Error('No logs found in transaction receipt');
        }

        // Parse the logs to find the PaymentSaved event
        let event;
        for (const log of receipt.logs) {
            try {
                const parsedLog = contract.interface.parseLog(log);
                if (parsedLog.name === 'PaymentSaved') {
                    event = parsedLog;
                    break;
                }
            } catch (error) {
                console.error('Error parsing log:', error);
            }
        }
        if (!event || event.name !== 'PaymentSaved') {
            throw new Error('PaymentSaved event not found in transaction logs');
        }

        const { paymentId, amount, paymentDate, invoiceIndices, paymentSolution: solution, status } = event.args;
        // Convert BigInt to string
        const paymentDetails = {
            paymentId: paymentId.toString(),
            amount: amount.toString(),
            paymentDate: paymentDate.toString(),
            invoiceIndices,
            paymentSolution: solution,
            status
        };
        console.log('Payments data written to the blockchain:', paymentDetails);
        return { message: 'Payments data written to the blockchain', ...paymentDetails };
    } catch (error) {
        throw new Error(`Error writing Payments in blockchain: ${error.message}`);
    }
}

async function updateExternalId(sc, paymentId, externalId) {
    try {
        const contract = await getContractConnection(sc)
        const tx = await contract.updateExternalId(paymentId, externalId);
        console.log(`Payment with id: ${paymentId} updated external id: ${externalId}`)
        return { message: `Payment with id: ${paymentId} updated external id: ${externalId}` };
    } catch (error) {
        throw new HttpError(`Error updating payment's external id: ${error.message}`, 500);
    }
}

async function getPayments(sc) {
    try {
        const contract = await getContractConnection(sc)
        const payments = await contract.getPayments();
        return payments.map(payment => ({
            paymentId: Number(payment.paymentId),
            amount: Number(payment.amount),
            paymentDate: new Date(Number(payment.paymentDate) * 1000),
            externalInvoiceId: payment.externalInvoiceId,
            paymentSolution: payment.paymentSolution,
            invoiceIndices: payment.invoiceIndices.split(';'),
            status: payment.status
        }));

    } catch (error) {
        throw new HttpError(`Error fetching Payments from blockchain: ${error.message}`, 500);
    }
}

async function confirmPayment(sc, paymentId) {
    try {
        const contract = await getContractConnection(sc)
        await contract.confirmPayment(paymentId);
        console.log(`Payment id: ${paymentId} confirmed successfully`);
    } catch (error) {
        console.error('Error confirming payment: ', error.message);
    }
}

module.exports = {
    getPayments,
    savePayment,
    getLinkedBilling,
    updateExternalId,
    confirmPayment
}