const logger = require('./logger');
const { ethers, JsonRpcProvider } = require("ethers");
const HttpError = require("../utils/httpError");
const axios = require('axios')
const scManagerUrl = process.env.SC_MANAGER_URL

async function getContractABI(contractDId) {
    try {
        const result = await axios.get(`${scManagerUrl}/smartContracts?contractDId=${contractDId}&label=billing`);
        return JSON.parse(result.data.infos.abi)
    } catch (error) {
        throw new HttpError(`Error fetching contract ABI: ${error.message}`, 500);

    }
}

async function getLinkedConsumptionSC(contractDId) {
    try {
        const result = await axios.get(`${scManagerUrl}/smartContracts?contractDId=${contractDId}&label=consumption`);
        return result.data.infos
    } catch (error) {
        throw new HttpError(`Error fetching linked address: ${error.message}`, 500);

    }
}

async function getContractConnection(sc) {
    const provider = new JsonRpcProvider(process.env.EVM_NODE_URL);
    const signer = new ethers.Wallet(process.env.PRIVATE_KEY, provider);
    const contractABI = await getContractABI(sc.contractDId);
    return new ethers.Contract(sc.address, contractABI, signer);
}

async function saveBill(sc, consumption, startDate, endDate) {
    try {
        const contract = await getContractConnection(sc);
        consumption.forEach(array => {
            array.splice(1, 1);
          });
        // console.log("Save Bill:", consumption, startDate, endDate);
        const tx = await contract.saveBill(consumption, Math.floor(startDate.getTime() / 1000), Math.floor(endDate.getTime() / 1000));
        await tx.wait();

        return { message: 'Billings data written to the blockchain' };
    } catch (error) {
        throw new HttpError(`Error writing bill in blockchain: ${error.message}`, 500);
    }
}

async function setCurrency(sc, currency) {
    try {
        const contract = await getContractConnection(sc);
        const tx = await contract.setCurrency(currency);
        await tx.wait();

        return { message: 'Currency set in the blockchain' };
    } catch (error) {
        throw new HttpError(`Error setting Currency in blockchain: ${error.message}`, 500);

    }
}

async function getPricingByType(sc, type) {
    try {
        const contract = await getContractConnection(sc);
        return await contract.getPricingByType(type);
    } catch (error) {
        throw new HttpError(`Error getting Pricing from blockchain: ${error.message}`, 500);
    }
}

async function setPricing(sc, pricings) {
    try {
        const contract = await getContractConnection(sc);
        await contract["setPricing((string,uint256,string,uint256)[])"](pricings);
        return { message: 'Pricing set in the blockchain' };
    } catch (error) {
        throw new HttpError(`Error setting Pricing in blockchain: ${error.message}`, 500);
    }
}

async function getBillsByPeriod(sc, startDate, endDate) {
    try {
        const contract = await getContractConnection(sc);
        const billIds = await contract.getBillsIdByPeriod(Math.floor(startDate.getTime() / 1000), Math.floor(endDate.getTime() / 1000));
        const bills = []
        for (let i = 0; i < billIds.length; i++) {
            const data = await contract.getBillById(billIds[i]);
            const details = [];

            for (let j = 0; j < data.details.length; j++) {
                details.push({
                    type: data.details[j].type_,
                    volume: Number(data.details[j].volume),
                    volumeType: data.details[j].volumeType,
                    pricePerVolume: Number(data.details[j].pricePerVolume),
                    totalAmount: Number(data.details[j].totalAmount),
                    volumeUnit: Number(data.details[j].volumeUnit),
                    pricePerVolume: Number(data.details[j].pricePerVolume)
                });
            }
            bills.push({
                id: Number(data.bill.id),
                creationDate: new Date(Number(data.bill.creationDate) * 1000),
                startDate: new Date(Number(data.bill.startDate) * 1000),
                endDate: new Date(Number(data.bill.endDate) * 1000),
                totalAmount: Number(data.bill.totalAmount),
                currency: data.bill.currency,
                details: JSON.stringify(details)
            });
        }

        return bills;
    } catch (error) {
        throw new HttpError(`Error fetching Bills by period from blockchain: ${error.message}`, 500);
    }
}

async function getBills(sc) {
    try {
        console.log(sc);
        const contract = await getContractConnection(sc);
        const count = await contract.getBillsCount();
        console.log(count);
        const bills = [];
        for (let i = 0; i < count; i++) {
            const data = await contract.getBillById(i);
            const details = [];
            for (let j = 0; j < data.details.length; j++) {
                details.push({
                    type: data.details[j].type_,
                    volume: Number(data.details[j].volume),
                    volumeType: data.details[j].volumeType,
                    pricePerVolume: Number(data.details[j].pricePerVolume),
                    totalAmount: Number(data.details[j].totalAmount),
                    volumeUnit: Number(data.details[j].volumeUnit),
                    pricePerVolume: Number(data.details[j].pricePerVolume)
                });
            }
            bills.push({
                id: Number(data.bill.id),
                creationDate: new Date(Number(data.bill.creationDate) * 1000),
                startDate: new Date(Number(data.bill.startDate) * 1000),
                endDate: new Date(Number(data.bill.endDate) * 1000),
                totalAmount: Number(data.bill.totalAmount),
                currency: data.bill.currency,
                details: JSON.stringify(details)
            });
        }
        return bills;
    } catch (error) {
        throw new HttpError(`Error fetching Bills from blockchain: ${error.message}`, 500);
    }
}

module.exports = {
    saveBill,
    getBills,
    getBillsByPeriod,
    setCurrency,
    setPricing,
    getPricingByType,
    getLinkedConsumptionSC
}