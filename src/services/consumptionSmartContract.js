const logger = require('./logger');
const { ethers, JsonRpcProvider } = require("ethers");
const HttpError = require("../utils/httpError");
const axios = require('axios')
const scManagerUrl = process.env.SC_MANAGER_URL

async function getContractABI(contractDId) {
    try {
        const result = await axios.get(`${scManagerUrl}/smartContracts?contractDId=${contractDId}&label=consumption`);
        return JSON.parse(result.data.infos.abi)
    } catch (error) {
        throw new HttpError(`Error fetching contract ABI: ${error.message}`, 500);

    }
}

async function getContractInfosFromDId(contractDId) {
    try {
        const result = await axios.get(`${scManagerUrl}/smartContracts?contractDId=${contractDId}&label=consumption`);

        return result.data.infos
    } catch (error) {
        throw new HttpError(`Error fetching consumption: ${error.message}`, 500);

    }
}


async function getContractConnection(sc) {
    const provider = new JsonRpcProvider(process.env.EVM_NODE_URL);
    const signer = new ethers.Wallet(process.env.PRIVATE_KEY, provider);
    const contractABI = await getContractABI(sc.contractDId);

    const contract = new ethers.Contract(sc.address, contractABI, signer);
    return contract;
}

async function SaveConsumption(sc, consumption) {
    try {
        const infos = await getContractInfosFromDId(sc.contractDId)
        const contract = await getContractConnection(infos)
        const tx = await contract.writeData(consumption);
        const receipt = await tx.wait();
        console.log('Consumption data written to the blockchain: ', consumption);
    } catch (error) {
        throw new Error(`Error writing Consumption in blockchain: ${error.message}`);
    }
}

async function GetConsumption(sc, startDate, endDate) {
    try {
        const infos = await getContractInfosFromDId(sc.contractDId)
        const contract = await getContractConnection(infos)
        const result = await contract.getDataByPeriod(Math.floor(startDate.getTime() / 1000), Math.floor(endDate.getTime() / 1000));
        return (result.map(item => [
            item[0],
            Number(item[1]),
            Number(item[2])
        ]));
    } catch (error) {
        throw new Error(`Error fetchun Consumption in blockchain: ${error.message}`);
    }
}

async function GetConsumption2(sc, startDate, endDate, next) {
    const infos = await getContractInfosFromDId(sc.contractDId)
    const contract = await getContractConnection(infos)
    console.log(startDate, endDate);
    const result = await contract.getDataByPeriod(startDate, endDate);
    next(result.map(item => [
        item[0],
        Number(item[1]),
        Number(item[2])
    ]));
}

module.exports = {
    SaveConsumption,
    GetConsumption,
    GetConsumption2
}