const cron = require('node-cron')
const axios = require('axios')
const HttpError = require("../utils/httpError");
billingSCService = require('../services/billingSmartContract');
consumptionSCService = require('../services/consumptionSmartContract');

const isoToCron = require("../utils/isoToCron");


async function fetchConsumptionsFromBlockchain(sc, startDate, endDate) {
    const consumption = await billingSCService.getLinkedConsumptionSC(sc.contractDId);
     const result = await consumptionSCService.GetConsumption(consumption, startDate, endDate);
     return result;
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
  }
  

async function billingProcess(sc, consumption, startDate, endDate) {
    try {
        
        // const consumptions = await fetchConsumptionsFromBlockchain(sc, startDate, endDate)
        const consumptions = [["pdf", 1719989393, getRandomInt(10)], ["pdf", 1719989395, getRandomInt(10)]]
        // console.log(startDate, endDate, "fetched consumptions :")
        // console.log(consumptions)
        console.log("payment {id: 0, external_id: SSFYVAIMTN} updated: PAID");
        await billingSCService.saveBill(sc, consumptions, startDate, endDate);
    } catch (error) {
        console.error('Error processing billings:', error.message)
    }
}

function startCronJob(req, next) {
    const cronExpression = isoToCron(req.body.periodicity);
    if (!cronExpression) {
        throw new Error('Invalid periodicity');
    }

    let previous = new Date();
    cron.schedule(cronExpression, () => {
        let now = new Date();
        // console.log(`Scheduled billing at (${req.body.address}) started (${req.body.periodicity})`)
        billingProcess({ address: req.body.address, contractDId: req.body.contractDId },
            "", previous, now);
        previous = new Date();
    });
    next()
}

async function saveBillManually(req, next) {
    try {
        await billingProcess({ address: req.body.address },
            req.body.consumption, req.body.startDate, req.body.endDate)
        next();
    } catch (error) {
        console.error('Error saving bill manually:', error.message)
        throw new HttpError('Error saving bill manually', 500);
    }
}

async function getBills(req, next) {
    try {
        const data = await billingSCService.getBills({ address: req.query.address  });
        next(data);
    } catch (error) {
        console.error('Error getting bills:', error.message)
        throw new HttpError('Error getting bills', 500);

    }
}

async function setPricing(req, next) {
    try {
        await billingSCService.setPricing({ address: req.body.address },
            req.body.pricing);
    } catch (error) {
        console.error('Error setting pricing:', error.message)
        throw new HttpError('Error setting pricing', 500);

    }
}

async function setCurrency(req, next) {
    try {
        billingSCService.setCurrency({ address: req.body.address },
            req.currency);
        next();
    } catch (error) {
        console.error('Error setting currency:', error.message)
        throw new HttpError('Error setting currency', 500);
    }
}

async function getBillsByPeriod(req, next) {
    try {
        const data = await billingSCService.getBillsByPeriod({ address: req.body.address },
            new Date(req.body.startDate), new Date(req.body.endDate));
        next(data)
    } catch (error) {
        console.error('Error getting bills by period:', error.message)
        throw new HttpError('Error getting bills by period', 500);

    }
}

module.exports = {
    startCronJob,
    getBills,
    setPricing,
    getBillsByPeriod,
    setCurrency,
    saveBillManually
}